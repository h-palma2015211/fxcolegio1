/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Materia;
import org.hugopalma.controller.ControllerMateria;

/**
 *
 * @author HUAL
 */
public class CRUDMateria extends CRUDPadre implements Interface{
    private static final CRUDMateria CRUD_MATERIA =  new
        CRUDMateria();
    
    
    private CRUDMateria(){
    }
    public static CRUDMateria getCRUD_MATERIA(){
       return CRUD_MATERIA;
    }
    private TableColumn<Materia, Integer> tableColumnidMateria;
    private TableColumn<Materia, String> tableColumnNombre;
    private TableView<Materia> tableView;
    private ObservableList observableList;
    
    
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        textTitle = new Text();
        
        gridPane.add(textTitle, 0,0);
        
        hBoxBuscar = new HBox(10);
        
        textField = new TextField();
        textField.setPromptText("Buscar Materia");
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               if (!textField.getText().trim().isEmpty()) {
                   String texto = textField.getText().trim();
                   buscar(texto);
               } else{
                   updateTableViewItems();
               }
            }
        });
        
        hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        agregar= new Button("Agregar");
        agregar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
               hBox.getChildren().clear();
               hBox.getChildren().addAll(gridPane, CreateMateria.getCREATE_MATERIA().getGridPane());
               updateObservableList();
               tableView.setItems(observableList);
            }
        });
        
        modificar = new Button("Modificar");
        modificar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                if(tableView.getSelectionModel().getSelectedItems() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateMateria.getUpdateMateria().getGridPane(tableView.getSelectionModel().getSelectedItem())); 
                    updateObservableList();
                    tableView.setItems(observableList);
                }
            }
        });
        
        eliminar = new Button("Eliminar");
        eliminar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                eliminar(tableView.getSelectionModel().getSelectedItem().getIdMateria());
                updateObservableList();
                tableView.setItems(observableList);
            }
        });
        
        hBoxButtons.getChildren().addAll(agregar, modificar, eliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnidMateria = new TableColumn<>();
        tableColumnidMateria.setText("ID");
        tableColumnidMateria.setCellValueFactory(new PropertyValueFactory<>("idMateria"));
        tableColumnidMateria.setMinWidth(150);
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        tableColumnNombre.setMinWidth(200);
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnidMateria, tableColumnNombre);
        
        gridPane.add(tableView, 0, 3);
        
        
        hBox.getChildren().add(gridPane);
        
        return hBox;
    }
    
    public void eliminar(int idMateria) {
        ControllerMateria.getCONTROLLER_MATERIA().delete(idMateria);
        updateObservableList();
    }
    
    
    
    public void updateObservableList(){
        observableList = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_MATERIA().getArrayList());
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_MATERIA().buscar(texto));
        tableView.setItems(observableList);
        
    }
    
    
}

class CreateMateria {
    public static CreateMateria CREATE_MATERIA = 
            new CreateMateria();
    
    private CreateMateria() {
    
    }
    
    public static CreateMateria getCREATE_MATERIA() {
        return CREATE_MATERIA;
    }
    
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textfieldAgregar;
    private Button button;
    
    public GridPane getGridPane() {
        gridPane  = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Agregar Materia");
        gridPane.add(textTitle, 0, 0);
        
        labelNombre = new Label("Nombre");
        gridPane.add(labelNombre, 0, 1);
        
        textfieldAgregar = new TextField();
        gridPane.add(textfieldAgregar, 1, 1);
        
        button = new Button("Agregar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                agregar(textfieldAgregar.getText());
            }
        });
        gridPane.add(button, 1, 3);
        
        return gridPane;
    }
    
    public void agregar(String nombre){
        ControllerMateria.getCONTROLLER_MATERIA().add(nombre);
        CRUDMateria.getCRUD_MATERIA().updateTableViewItems();
    }
    
    
        

}


class UpdateMateria{
    public static final UpdateMateria UPDATE_MATERIA = new 
        UpdateMateria();
    
    private UpdateMateria() {
    
    }
    public static UpdateMateria getUpdateMateria() {
        return UPDATE_MATERIA;
    }
    
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textfield;
    private Button button;
    
    public GridPane getGridPane(Materia materia) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Modificar");
        gridPane.add(textTitle, 0, 0);
        
        
        labelNombre = new Label("Nombre");
        gridPane.add(labelNombre, 0, 1);
        
        textfield = new TextField();
        textfield.setText(materia.getNombre());
        gridPane.add(textfield, 1, 1);
        
        
        button = new Button("Modificar");
        button.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                modificar(textfield.getText(), materia.getIdMateria());
            }
        });
         
        gridPane.add(button, 1, 2);
        return gridPane;
        
        
    }
    
    public void modificar(String nombre, int idMateria) {
    ControllerMateria.getCONTROLLER_MATERIA().modify(nombre, idMateria);
    CRUDMateria.getCRUD_MATERIA().updateTableViewItems();
    }
}
