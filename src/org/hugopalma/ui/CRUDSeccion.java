/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Alumno;
import org.hugopalma.bean.Grado;
import org.hugopalma.bean.Jornada;
import org.hugopalma.bean.Seccion;
import org.hugopalma.bean.SeccionAlumno;
import org.hugopalma.bean.Actividad;
import org.hugopalma.bean.Materia;
import org.hugopalma.controller.ControllerActividad;
import org.hugopalma.controller.ControllerAlumno;
import org.hugopalma.controller.ControllerActividadAlumno;
import org.hugopalma.controller.ControllerGrado;
import org.hugopalma.controller.ControllerJornada;
import org.hugopalma.controller.ControllerMateria;
import org.hugopalma.controller.ControllerSeccion;
import org.hugopalma.controller.ControllerSeccionAlumno;



/**
 *
 * @author HUAL
 */
public class CRUDSeccion extends CRUDPadre {
    private static final CRUDSeccion CRUD_SECCION = new CRUDSeccion();
   
    private CRUDSeccion() {
    }
    
    public static CRUDSeccion getCRUD_SECCION() {
        return CRUD_SECCION;
    }
    
    private TableColumn<Seccion, Integer> tableColumnId;
    private TableColumn<Seccion, String> tableColumnNombre;
    private TableColumn<Seccion, Grado> tableColumnGrado;
    private TableColumn<Seccion, Jornada> tableColumnJornada;
    private ObservableList observableList;
    private TableView<Seccion> tableView;
    private Button button2;
    private Button button3;
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Secciones");
        gridPane.add(textTitle, 0, 0);
        hBoxBuscar  = new HBox(10);
        textField = new TextField();
        textField.setPromptText("Buscar Seccion");
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!textField.getText().trim().isEmpty()) {
                    String texto = textField.getText().trim();
                    buscar(texto);
                } else {
                    updateTableViewItems();
                }
            }
        }); 
        hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        agregar = new Button("Agregar");
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               hBox.getChildren().clear();
               hBox.getChildren().addAll(gridPane, CreateSeccion.getCREATE_SECCION().getGridPane());
            }
        });
        
        modificar = new Button("Modificar");
        modificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateSeccion.getUPDATE_SECCION().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                    updateObservableList();
                }
            }
        });
        
        eliminar = new Button("Eliminar");
        eliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    eliminar(tableView.getSelectionModel().getSelectedItem().getIdSeccion());
                }
            }
        });
        
        button2 = new Button("ALUMNOS");
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, CRUDListado.getCRUD_LISTADO().getGridPane(tableView.getSelectionModel().getSelectedItem().getIdSeccion()));
                    
                }
            }
        });
        button3 = new Button("LISTADO");
        button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, ActividadesAlumno.getACTIVIDADES_ALUMNO().getgridPane(tableView.getSelectionModel().getSelectedItem().getIdSeccion()));
                }
            }
        }); 
        
        
        hBoxButtons.getChildren().addAll(agregar, modificar, eliminar, button2, button3);
        
        gridPane.add(hBoxButtons, 0, 2);
         
        
        tableColumnId = new TableColumn<>("ID");
        tableColumnId.setCellValueFactory(new PropertyValueFactory<>("idSeccion"));
        
        tableColumnNombre = new TableColumn<>("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        tableColumnGrado = new TableColumn<>("Grado");
        tableColumnGrado.setCellValueFactory(new PropertyValueFactory<>("Grado"));
        
        tableColumnJornada = new TableColumn<>("Jornada");
        tableColumnJornada.setCellValueFactory(new PropertyValueFactory<>("Jornada"));
       
        updateObservableList();
        
        tableView = new TableView<>(observableList);
        tableView.setMaxWidth(300);
        tableView.getColumns().addAll(tableColumnId, tableColumnNombre,tableColumnGrado, tableColumnJornada);
        gridPane.add(tableView, 0, 3, 2, 1);
        
        hBox.getChildren().add(gridPane);
        return hBox;
         
    }
    
    public void eliminar(int idUsuario) {
        ControllerSeccion.getCONTROLLER_SECCION().delete(idUsuario);
        updateTableViewItems();
    }
    
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_SECCION().buscar(texto));
        tableView.setItems(observableList);
        
    }
    public void updateObservableList() {
       observableList = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_SECCION().getArrayList());
    }
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    
    
}

class CreateSeccion {
    private static final CreateSeccion CREATE_SECCION = new CreateSeccion();
    
    private CreateSeccion() {
    }
    
    public static CreateSeccion getCREATE_SECCION(){
        return CREATE_SECCION;
    }
    
    private GridPane gridPane;
    private Text textTitle;
    private Label label1;
    private TextField textField1;
    private ComboBox<Grado> comboBox;
    private ComboBox<Jornada> comboBox2;
    private ObservableList<Jornada> observableListJornada;
    private Button button;
    private ObservableList<Grado> observableListGrado;
    
    
    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Agregar");
        gridPane.add(textTitle, 0, 0);
        
        UpdateObservableListGrado();
        comboBox = new ComboBox<>(observableListGrado);
        gridPane.add(comboBox, 0, 1);
        
        UpdateObservableListJornada();
        comboBox2 = new ComboBox<>(observableListJornada);
        gridPane.add(comboBox2, 0, 2);
        
        label1 = new Label("nombre");
        gridPane.add(label1, 0, 3);
        
        textField1 = new TextField();
        gridPane.add(textField1, 1, 3);
        
        button = new Button("Agregar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                agregar(comboBox.getSelectionModel().getSelectedItem(),
                        textField1.getText(), comboBox2.getSelectionModel().getSelectedItem());
            }
        }); 
        gridPane.add(button, 1, 4);
        
        return gridPane;
        
        
    }
    
    public void UpdateObservableListGrado() {
        observableListGrado = FXCollections.observableArrayList(ControllerGrado.getCONTROLLER_GRADO().getArrayList());
    }
    
    public void agregar(Grado grado, String nombre, Jornada jornada) {
        ControllerSeccion.getCONTROLLER_SECCION().add(nombre, grado.getIdGrado(),jornada.getIdJornada());
        CRUDSeccion.getCRUD_SECCION().updateTableViewItems();
    }
    public void UpdateObservableListJornada() {
        observableListJornada = FXCollections.observableArrayList(ControllerJornada.getCONTROLLER_JORNADA().getArrayList());
    }
    

}

class UpdateSeccion {
    private static final UpdateSeccion UPDATE_SECCION = new UpdateSeccion();
    
    private UpdateSeccion() {
    }
    
    public static UpdateSeccion getUPDATE_SECCION() {
        return UPDATE_SECCION;
    }
    
    private GridPane gridPane;
    private Text textTitle;
    private ComboBox<Grado> comboBox;
    private Label labelNombre;
    private TextField textField;
    private Button button;
    private ObservableList<Grado> observableListGrado;
    private ObservableList<Jornada> observableListJornada;
    private ComboBox<Jornada> comboBox2;
    private Label label1;
    private Label label2;
    
    public GridPane getGridPane(Seccion seccion) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Seccion");
        gridPane.add(textTitle, 0, 0);
        
        label2 = new Label("Grado");
        gridPane.add(label2, 0, 1);
        UpdateObservableListGrado();
        comboBox = new ComboBox<>(observableListGrado);
        gridPane.add(comboBox, 1, 1);
        
        label1 = new Label("Jornada");
        gridPane.add(label1, 0, 1);
        UpdateObservableListJornada();
        comboBox2 = new ComboBox<>(observableListJornada);
        gridPane.add(comboBox2, 1, 2);
        
        labelNombre = new Label("Nombre");
        gridPane.add(labelNombre, 0, 3);
        
        textField = new TextField();
        textField.setText(seccion.getNombre());
        gridPane.add(textField, 1, 3);
        
        button = new Button("modificar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                modificar(textField.getText(), comboBox.getSelectionModel().getSelectedItem(), comboBox2.getSelectionModel().getSelectedItem(), seccion.getIdSeccion());
            }
        }); 
        gridPane.add(button, 1, 4);
        
        return gridPane;
    }
    
    public void UpdateObservableListGrado() {
        observableListGrado = FXCollections.observableArrayList(ControllerGrado.getCONTROLLER_GRADO().getArrayList());
    }
    
    public void modificar(String nombre, Grado grado, Jornada jornada, int idSeccion) {
        ControllerSeccion.getCONTROLLER_SECCION().modify(nombre, grado.getIdGrado(), idSeccion, jornada.getIdJornada());
        CRUDSeccion.getCRUD_SECCION().updateTableViewItems();
    }
    public void UpdateObservableListJornada() {
        observableListJornada = FXCollections.observableArrayList(ControllerJornada.getCONTROLLER_JORNADA().getArrayList());
    }
} 

class CRUDListado{
   private static final CRUDListado CRUD_LISTADO = new CRUDListado();
   
  private CRUDListado() {
  }
  public static CRUDListado getCRUD_LISTADO() {
      return CRUD_LISTADO;
  }
  
  private ObservableList<Alumno> observableListAlumno;
  private ListView<Alumno> listView;
  private HBox hBox2;
  private ObservableList<Alumno> observableList2;
  private ListView<Alumno> listView2;
  private HBox hBox;
  private GridPane gridPane;
  private Button button1;
  private Button button2;
  private Button button3;
  private VBox vBox;
  private ComboBox<Materia> comboBox;
  
  public GridPane getGridPane(int idSeccion) {
      gridPane = new GridPane();
      gridPane.setHgap(10);
      gridPane.setVgap(10);
      gridPane.setPadding(new Insets(25, 25, 25, 25));
      hBox = new HBox(10);
   
      UpdateObservableList2(idSeccion);
      listView = new ListView<>(observableList2); 
      hBox.getChildren().add(listView);
      gridPane.add(hBox, 0, 1);
      
      vBox = new VBox(10);
     
      button1 = new Button("AGREGAR"); 
      button1.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) {
              if (listView.getSelectionModel().getSelectedItem() != null) {
                  ControllerSeccionAlumno.getControllerSeccionAlumno().add(idSeccion, listView.getSelectionModel().getSelectedItem().getIdAlumno());
                  UpdateObservableList2(idSeccion);
                  UpdateObservableListAlumno(idSeccion);
                  listView.setItems(observableList2);
                  listView2.setItems(observableListAlumno);
              }
          }
      });
      
      
      button3 = new Button("ELIMINAR"); 
      button3.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) {
              if (listView2.getSelectionModel().getSelectedItem() != null) {
                  ControllerSeccionAlumno.getControllerSeccionAlumno().delete(listView2.getSelectionModel().getSelectedItem().getIdAlumno());
                  UpdateObservableList2(idSeccion);
                  UpdateObservableListAlumno(idSeccion);
                  listView.setItems(observableList2);
                  listView2.setItems(observableListAlumno);
              
              }
          }
      }); 
      
      vBox.getChildren().addAll(button1, button3);
      gridPane.add(vBox, 2, 1);
      
      hBox2 = new HBox(10);
      listView2 = new ListView<>();
      UpdateObservableListAlumno(idSeccion);
      listView2.getItems().setAll(observableListAlumno);
      hBox2.getChildren().add(listView2);
      gridPane.add(hBox2, 3, 1);
      
      return gridPane;
  }
  
  public void UpdateObservableListAlumno(int idSeccion) {
    
      observableListAlumno = FXCollections.observableArrayList(ControllerAlumno.getCONTROLLER_ALUMNO().getArrayList2(idSeccion));
  }
  public void UpdateObservableList2(int idSeccion) {
      observableList2 = FXCollections.observableArrayList(ControllerAlumno.getCONTROLLER_ALUMNO().getArrayList3(idSeccion));
  }
    
  

}

class ActividadesAlumno {
    private static final ActividadesAlumno ACTIVIDADES_ALUMNO 
            = new ActividadesAlumno();
    
    private ActividadesAlumno() {
    }
    
    public static ActividadesAlumno getACTIVIDADES_ALUMNO() {
        return ACTIVIDADES_ALUMNO;
    }
    
    private GridPane gridPane1;
    private GridPane gridPane;
    private ObservableList<Alumno> observableListAlumno;
    private ListView<Alumno> listView;
    private ListView<Actividad> listView2;
    private ListView<Actividad> listView3;
    private ObservableList<Actividad> observableListActividad;
    private ObservableList<Actividad> observableListActividad2;
    private ObservableList<Materia> observableListMateria;
    private ComboBox<Materia> comboBox;
    private HBox hbox;
    private Button button;
    private VBox vBox;
    private Button boton;
    private Text texto;
    private Text texto1;
    private Text texto2;
    private Button button2;
    private Button button3;
    private TextField textField;
    private Button calificar;
    private VBox vBox1;
    
    public GridPane getgridPane(int idSeccion) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
    
        UpdateObservableListAlumno(idSeccion);
        listView = new ListView<>();
        listView.getItems().setAll(observableListAlumno);
        
        hbox = new HBox(10);
        
        texto = new Text("Alumnos");
        hbox.getChildren().addAll(listView);
        hbox.setMaxWidth(140);
        hbox.setMaxHeight(300);
        
        gridPane.add(hbox, 0, 0);
        gridPane.add(texto, 0, 1);
        button = new Button("MATERIAS");
        gridPane.add(button, 1, 1);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (listView.getSelectionModel().getSelectedItem() != null) {
                    gridPane.add(getGridPane(listView.getSelectionModel().getSelectedItem().getIdAlumno()), 2, 0);
                }
            }
        }); 
        
        return gridPane;
    }
    
    public void UpdateObservableListAlumno(int idSeccion) {
    
      observableListAlumno = FXCollections.observableArrayList(ControllerAlumno.getCONTROLLER_ALUMNO().getArrayList2(idSeccion));
    }
    public GridPane getGridPane(int idAlumno) {
        gridPane1 = new GridPane();
        gridPane1.setHgap(10);
        gridPane1.setVgap(10);
        gridPane1.setPadding(new Insets(25, 25, 25, 25));
        
        vBox1 = new VBox(10);
        ObservableListMateria();
        comboBox = new ComboBox<>(observableListMateria);
        vBox1.getChildren().add(comboBox);
        boton = new Button("Actividades");
        boton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gridPane1.add(gethBox(idAlumno, comboBox.getSelectionModel().getSelectedItem().getIdMateria()), 1, 0);
            }
        }); 
        vBox1.getChildren().add(boton);
        texto2 = new Text("MATERIAS");
        vBox1.getChildren().add(texto2);
        gridPane.add(vBox1, 1, 0);
        return gridPane1;
    }
    public void ObservableListMateria() {
        observableListMateria = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_MATERIA().getArrayList());
    } 
    
    
    
    public HBox gethBox(int idAlumno, int idMateria) {
       hbox = new HBox();
        
       UpdateObservableListActividad1(idAlumno, idMateria);
       listView2 = new ListView<>(observableListActividad);
       listView2.setMaxHeight(150);
       listView2.setMaxWidth(100); 
       hbox.getChildren().add(listView2);
       
       vBox = new VBox(10);
       button2 = new Button("Asignar");
       button2.setOnAction(new EventHandler<ActionEvent>() {
           @Override
           public void handle(ActionEvent event) {
               if (listView2.getSelectionModel().getSelectedItem() != null) {
                   ControllerActividadAlumno.getCONTROLLER_ACTIVIDAD_ALUMNO().add(listView2.getSelectionModel().getSelectedItem().getIdActividad(), idAlumno);
                   UpdateObservableListActividad1(idAlumno, idMateria);
                   listView2.setItems(observableListActividad);
                   UpdateObservableListActividad2(idAlumno, idMateria);
                   listView3.setItems(observableListActividad2);
               }
           }
       }); 
       button3 = new Button("Remover");
       button3.setOnAction(new EventHandler<ActionEvent>() {
           @Override
           public void handle(ActionEvent event) {
               if (listView3.getSelectionModel().getSelectedItem() != null) {
                   ControllerActividadAlumno.getCONTROLLER_ACTIVIDAD_ALUMNO().delete(listView3.getSelectionModel().getSelectedItem().getIdActividad());
                   UpdateObservableListActividad2(idAlumno, idMateria);
                   listView3.setItems(observableListActividad2);
                   UpdateObservableListActividad1(idAlumno, idMateria);
                   listView2.setItems(observableListActividad);
               }
           }
       }); 
       vBox.getChildren().addAll(button2, button3);
       hbox.getChildren().add(vBox);
       
        UpdateObservableListActividad2(idAlumno, idMateria);
        listView3 = new ListView<>(observableListActividad2);
        listView3.setMaxHeight(150);
        listView3.setMaxWidth(100); 
        hbox.getChildren().add(listView3);
        
        textField = new TextField();
        textField.setVisible(false); 
        textField.setPromptText("Nota Obtenida");
        textField.setMaxWidth(100);
        calificar = new Button("CALIFICAR");
        calificar.setVisible(false);
        hbox.getChildren().addAll(textField, calificar);    
        listView3.setOnMouseClicked(new EventHandler<MouseEvent>() {
           @Override
           public void handle(MouseEvent event) {
               if (listView3.getSelectionModel().getSelectedItem() != null) {
                   textField.setVisible(true);
                   calificar.setVisible(true);
            
               }
           }
       });
        
       return hbox;
       
    }
    
    public void UpdateObservableListActividad1(int idAlumno, int idMateria) {
        observableListActividad = FXCollections.observableArrayList(ControllerActividad.getCONTROLLER_ACTIVIDAD().getArrayList3(idAlumno, idMateria));
    }
    public void UpdateObservableListActividad2(int idAlumno, int idMateria) {
        observableListActividad2 = FXCollections.observableArrayList(ControllerActividad.getCONTROLLER_ACTIVIDAD().getArrayList2(idAlumno, idMateria));
    }
    
}


