/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Usuario;
import org.hugopalma.controller.ControllerUsuario;

/**
 *
 * @author HUAL
 */
public class CRUDUsuario extends CRUDPadre implements Interface {
    private static final CRUDUsuario CRUD_USUARIO =
            new CRUDUsuario();
    

    
    
    private TableColumn<Usuario, Integer> tableColumnIdUsuario;
    private TableColumn<Usuario, String> tableColumnNombre;
    private TableColumn<Usuario, String> tableColumnClave;
    private TableView<Usuario> tableView;
    private ObservableList observableList;
    
    private CRUDUsuario() {

    }
    public static CRUDUsuario getCRUD_USUARIO() {
        return CRUD_USUARIO;
    }
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        
        textTitle = new Text("USUARIOS");
        
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textField = new TextField();
        textField.setPromptText("Buscar Usuario");
       
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!textField.getText().trim().isEmpty()) {
                String text = textField.getText().trim();    
                buscar(text);
                }else{
                    updateTableViewItems();
                }
            }
        });
        hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        agregar = new Button("Nuevo");
        
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, CreateUsuario.get_CREATE_USUARIO().getGridPane());
            }
        });
        modificar = new Button("Modificar");
        modificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateUsuario.getUpdateUsuario().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                    updateObservableList();
                }
            }
        });
        eliminar = new Button("Eliminar");
        eliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    eliminar(tableView.getSelectionModel().getSelectedItem().getIdUsuario());
                    updateObservableList();
                    tableView.setItems(observableList);
                }
            }
        });
        
        hBoxButtons.getChildren().addAll(agregar, modificar,eliminar);
        
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdUsuario = new TableColumn<>();
        tableColumnIdUsuario.setText("ID");
        tableColumnIdUsuario.setCellValueFactory(new PropertyValueFactory<>("idUsuario"));
        tableColumnIdUsuario.setMinWidth(150);
        
        
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        
        
        tableColumnClave = new TableColumn<>();
        tableColumnClave.setText("Clave");
        tableColumnClave.setCellValueFactory(new PropertyValueFactory<>("clave"));
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdUsuario, tableColumnNombre, tableColumnClave);
        gridPane.add(tableView, 0, 3, 2, 1);
        
        hBox.getChildren().add(gridPane);
        
        return hBox;
        
        
        
        
    }
    public void eliminar(int idUsuario) {
        ControllerUsuario.getCONTROLLER_USUARIO().delete(idUsuario);
        updateObservableList();
    }
    
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerUsuario.getCONTROLLER_USUARIO().buscar(texto));
        tableView.setItems(observableList);
        
    }
    public void updateObservableList() {
       observableList = FXCollections.observableArrayList(ControllerUsuario.getCONTROLLER_USUARIO().getArrayList());
    }
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    
}

class CreateUsuario {
    private static final CreateUsuario CREATE_USUARIO = 
            new CreateUsuario();
    
    private CreateUsuario() {
    }
    
    public static CreateUsuario get_CREATE_USUARIO() {
        return CREATE_USUARIO;
    }
    
    private GridPane gridPane1;
    private Text textTitulo;    
    private Label labelNombre;
    private Label labelClave;
    private PasswordField passwordField;
    private TextField textFi;
    
    private Button buttonAgregar;
    
    
    
    
    public GridPane getGridPane() {
        gridPane1 = new GridPane();
        gridPane1.setHgap(10);
        gridPane1.setVgap(10);
        gridPane1.setPadding(new Insets(25, 25, 25, 25));
        
        textTitulo = new Text("Agregar Usuario");
        gridPane1.add(textTitulo, 0, 0);
        
        labelNombre = new Label("Usuario");
        gridPane1.add(labelNombre, 0, 1);
        
        textFi = new TextField();
        gridPane1.add(textFi, 1, 1);
        
        labelClave = new Label("Clave");
        gridPane1.add(labelClave, 0, 2);
        passwordField = new  PasswordField();
        gridPane1.add(passwordField, 1, 2);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                agregar(textFi.getText(), passwordField.getText());
                
            }
        });
        gridPane1.add(buttonAgregar, 1, 3);
        
        return gridPane1;
        
    }
    
    public void agregar(String nombre, String clave){
        ControllerUsuario.getCONTROLLER_USUARIO().add(nombre, clave);
        CRUDUsuario.getCRUD_USUARIO().updateTableViewItems();
    }
    


}

class UpdateUsuario {
    private static final UpdateUsuario UPDATE_USUARIO = new UpdateUsuario();
    
    private UpdateUsuario(){
      
    }
    
    public static UpdateUsuario getUpdateUsuario(){
        return UPDATE_USUARIO;
    }
    
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordField;
    private Button button;
    
    public GridPane getGridPane(Usuario usuario){
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Modificar");
        gridPane.add(textTitle, 0, 0);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 1);
        
        textFieldNombre = new TextField();
        textFieldNombre.setText(usuario.getNombre());
        gridPane.add(textFieldNombre, 1, 1);
        
        labelClave = new Label("Clave");
        gridPane.add(labelClave, 0, 2);
        
        passwordField = new PasswordField();
        passwordField.setText(usuario.getClave());
        gridPane.add(passwordField, 1, 2);
        
        button = new Button("Modificar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                modificar(textFieldNombre.getText(), passwordField.getText(),usuario.getIdUsuario());
            }
        });
        gridPane.add(button, 1, 3);
        
        return gridPane;
        
    }
    
    public void modificar(String nombre, String clave, int idUsuario) {
        ControllerUsuario.getCONTROLLER_USUARIO().modify(nombre, clave, idUsuario);
        CRUDUsuario.getCRUD_USUARIO().updateTableViewItems();
    }
    
    

}