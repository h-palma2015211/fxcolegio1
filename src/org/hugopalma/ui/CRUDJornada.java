/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Jornada;
import org.hugopalma.controller.ControllerJornada;
/**
 *
 * @author HUAL
 */
public class CRUDJornada extends CRUDPadre implements Interface {
    private static final CRUDJornada CRUD_JORNADA = new CRUDJornada();

    
    private TableColumn<Jornada, Integer> tableColumnIdJornada;
    private TableColumn<Jornada, String> tableColumnNombre;
    private TableView<Jornada> tableView;
    private ObservableList observableList;
    
    private CRUDJornada() {
    }
    public static CRUDJornada getCRUD_JORNADA() {
        return CRUD_JORNADA;
    }
    
    public HBox gethBox() {
        hBox = new HBox();
        
       gridPane = new GridPane();
       gridPane.setHgap(10);
       gridPane.setVgap(10);
       
       
       textTitle = new Text("Jornada");
       gridPane.add(textTitle, 0, 0);
       
       hBoxBuscar = new HBox(10);
       textField = new TextField();
       textField.setPromptText("Buscar Jornada");
       
       buttonBuscar = new Button("Buscar");
       buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!textField.getText().trim().isEmpty()) {
                    String text = textField.getText().trim();
                    buscar(text);
                } else {
                    updateTableViewItems();
                }
                
            }
        });
        hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        agregar = new Button("Nuevo");
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, CreateJornada.getCREATE_JORNADA().getGridPane());
                
            }
        });
         
        modificar = new Button("Modificar");
        modificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateJornada.getUPDATE_JORNADA().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                    updateObservableList();
                }
            }
        }); 

        eliminar = new Button("Elminar");
        eliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    eliminar(tableView.getSelectionModel().getSelectedItem().getIdJornada());
                    updateObservableList();
                    tableView.setItems(observableList);
                }
            }
        }); 
        
        hBoxButtons.getChildren().addAll(agregar, modificar, eliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdJornada = new TableColumn<>();
        tableColumnIdJornada.setText("ID");
        tableColumnIdJornada.setCellValueFactory(new PropertyValueFactory<>("idJornada"));
        tableColumnIdJornada.setMinWidth(150);
        
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
        tableColumnNombre.setMinWidth(300);
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdJornada, tableColumnNombre);
        gridPane.add(tableView, 0, 3, 2, 1);
        hBox.getChildren().add(gridPane);
        
        return hBox;
        
        
        
    }
    public void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerJornada.getCONTROLLER_JORNADA().getArrayList());
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    public void eliminar(int id) {
        ControllerJornada.getCONTROLLER_JORNADA().delete(id);
        updateTableViewItems();
    }
    
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerJornada.getCONTROLLER_JORNADA().buscar(texto));
        tableView.setItems(observableList);
        
    }
}

class CreateJornada {
    private static final CreateJornada CREATE_JORNADA  = new CreateJornada();
    
    private CreateJornada() {
    }
    public static CreateJornada getCREATE_JORNADA() {
        return CREATE_JORNADA;
    }
    
    private Text textTitulo;
    private Label nombre;
    private TextField textField;
    private GridPane gridPane;
    private Button button;
    
    
    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitulo = new Text("Agregar");
        gridPane.add(textTitulo, 0, 0);
        
       nombre = new Label("Nombre");
       gridPane.add(nombre, 0, 1);
       
       textField = new TextField();
       gridPane.add(textField, 1, 1);
       
       button = new Button("Agregar");
       button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               agregar(textField.getText());
            }
        });
       gridPane.add(button, 1, 2);
       
       return gridPane;
    
    }
    
    
    public void agregar(String nombre) {
        ControllerJornada.getCONTROLLER_JORNADA().add(nombre);
        CRUDJornada.getCRUD_JORNADA().updateTableViewItems();
    }
    
}


class UpdateJornada{
    private static final UpdateJornada UPDATE_JORNADA   = new UpdateJornada();
    
    private UpdateJornada() {
    
    }
    
    public static UpdateJornada getUPDATE_JORNADA() {
        return UPDATE_JORNADA;
    } 
    
    private GridPane gridPane;
    private Text textoTitulo;
    private Label label;
    private TextField textField;
    private Button button;
    
    public GridPane getGridPane(Jornada jornada) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        textoTitulo = new Text("Modificar");
        gridPane.add(textoTitulo, 0, 0);
        
        label = new Label("Nombre");
        gridPane.add(label, 0, 1);
        
        textField = new TextField();
        textField.setText(jornada.getNombre());
        gridPane.add(textField, 1, 1);
        
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
             modificar(textField.getText(), jornada.getIdJornada());   
            }
        });
         gridPane.add(button, 1, 2);
         
         return gridPane;
    
    }
    
    public void modificar(String nombre, int id) {
        ControllerJornada.getCONTROLLER_JORNADA().modify(nombre, id);
        CRUDJornada.getCRUD_JORNADA().updateTableViewItems();
    }
}