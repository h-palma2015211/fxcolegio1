/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Alumno;
import org.hugopalma.controller.ControllerAlumno;
import org.hugopalma.controller.ControllerUsuario;

/**
 *
 * @author HUAL
 */
public class CRUDAlumno extends CRUDPadre {
    private static final CRUDAlumno CRUD_ALUMNO = new CRUDAlumno();
    
    private CRUDAlumno() {
    }
   
    public static CRUDAlumno getCRUD_ALUMNO() {
        return CRUD_ALUMNO;
    }
    
    
    
    private TableColumn<Alumno, Integer> tableColumnIdAlumno;
    private TableColumn<Alumno, String> tableColumnNombre;
    private TableView<Alumno> tableView;
    private ObservableList observableList;
    
    
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        textTitle = new Text("ALUMNOS");
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textField = new TextField();
        textField.setPromptText("Buscar Alumno");
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!textField.getText().trim().isEmpty()) {
                   String text = textField.getText().trim();
                   buscar(text);
                } else {
                    updateTableViewItems();
                }
            }
        }); 
        hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        agregar = new Button("Agregar");
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, CreateAlumno.getCREATE_ALUMNO().getGridPane());
            }
        });
        
        modificar = new Button("Modificar");
        modificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateAlumno.getUpdateAlumno().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                    updateObservableList();
                }
            }
        });
        
        eliminar = new Button("Eliminar");
        eliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null ) {
                    eliminar(tableView.getSelectionModel().getSelectedItem().getIdAlumno());
                    updateObservableList();
                    tableView.setItems(observableList);
                }
            }
        }); 
        
        hBoxButtons.getChildren().addAll(agregar, modificar, eliminar);
        
        gridPane.add(hBoxButtons, 0, 2);
      
        
        tableColumnIdAlumno = new TableColumn<>("ID");
        tableColumnIdAlumno.setCellValueFactory(new PropertyValueFactory<>
        ("idAlumno"));
        tableColumnIdAlumno.setMinWidth(100);
        
        tableColumnNombre = new TableColumn<>("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>
        ("nombre"));
        tableColumnNombre.setMinWidth(250);
        updateObservableList();
        
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdAlumno, tableColumnNombre);
        
        gridPane.add(tableView, 0, 3, 2, 1);
        
        
        hBox.getChildren().add(gridPane);
        
        return hBox;
    }
    
    
    public void eliminar(int idUsuario) {
        ControllerAlumno.getCONTROLLER_ALUMNO().delete(idUsuario);
        updateObservableList();
    }
    
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerAlumno.getCONTROLLER_ALUMNO().buscar(texto));
        tableView.setItems(observableList);
        
    }
    public void updateObservableList() {
       observableList = FXCollections.observableArrayList(ControllerAlumno.getCONTROLLER_ALUMNO().getArrayList());
    }
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
}

class CreateAlumno {
    private static final CreateAlumno CREATE_ALUMNO = new CreateAlumno();
    
    private CreateAlumno(){
    }
    
    public static CreateAlumno getCREATE_ALUMNO() {
        return CREATE_ALUMNO;
    }
    private GridPane gridPane;
    private Text text;
    private Label label;
    private TextField textField;
    private Button button;
    
    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        text = new Text("AGREGAR");
        gridPane.add(text, 0, 0);
        
        label = new Label("Nombre");
        gridPane.add(label, 0, 1);
        
        textField = new TextField();
        gridPane.add(textField, 1, 1);
        
        button = new Button("Agregar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                agregar(textField.getText());
            }
        });
        gridPane.add(button, 1, 2);
         
        return gridPane;
    }
    
    public void agregar(String nombre) {
        ControllerAlumno.getCONTROLLER_ALUMNO().add(nombre);
        CRUDAlumno.getCRUD_ALUMNO().updateTableViewItems();
    }
    

}

class UpdateAlumno {
    private static final UpdateAlumno UPDATE_ALUMNO = new UpdateAlumno();
    
    private UpdateAlumno(){ 
    }
    
    public static UpdateAlumno getUpdateAlumno() {
         return UPDATE_ALUMNO;
    }
    
    private GridPane gridPane;
    private Text text;
    private Label label;
    private TextField textField;
    private Button button;
    
    
    
    public GridPane getGridPane(Alumno alumno) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        text = new Text("Modificar");
        gridPane.add(text, 0, 0);
        
        label = new Label("Nombre");
        gridPane.add(label, 0, 1);
        
        textField = new TextField();
        textField.setText(alumno.getNombre());
        gridPane.add(textField, 1, 1);
        
        button = new Button("Modificar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                modificar(textField.getText(), alumno.getIdAlumno());
            }
        }); 
        gridPane.add(button, 1, 2);
        
        return gridPane;
        
        
    }
    
    public void modificar(String nombre, int idAlumno) {
        ControllerAlumno.getCONTROLLER_ALUMNO().modify(nombre, idAlumno);
        CRUDAlumno.getCRUD_ALUMNO().updateTableViewItems();
    }
    

}