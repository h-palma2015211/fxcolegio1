/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author HUAL
 */
public class Dashboard {
    private static final Dashboard DASHBOARD = new Dashboard();
    private HBox hBox;
    private Text textTitle;
    private Image image;
    private ImageView ver;
    private Dashboard() {
    
    }
    
    public static Dashboard getDASHBOARD() {
        return DASHBOARD;
    }
    
    public HBox getHbox() {
        hBox = new HBox();
        hBox.setId("hBox");
        
        image = new Image("/org/hugopalma/resource/1024px-New_England_Patriots_logo_old.svg.png");
        ver = new ImageView();
        ver.setFitHeight(650);
        ver.setFitWidth(650);
        ver.setX(10);
        ver.setY(10);
        ver.setImage(image);
        
        StackPane stackPane = new StackPane();
        stackPane.setId("stackPane");
        stackPane.setMinSize(500, 250);
        hBox.getChildren().addAll(ver,stackPane);
        
        
        VBox vBox = new VBox();
        vBox.setId("vBox");
        vBox.setMinSize(500,200);
        hBox.getChildren().addAll(vBox);
        
        
        return hBox;
        
        
        
        
 
    }
    
}
