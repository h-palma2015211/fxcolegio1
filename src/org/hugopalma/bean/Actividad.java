/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class Actividad {
    private int idActividad;
    private String descripcion;
    private int valor;
    private short bimestre;
    private Materia materia;

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setBimestre(short bimestre) {
        this.bimestre = bimestre;
    }

    public short getBimestre() {
        return bimestre;
    }

    public void setIdActividad(int idActividad) {
        this.idActividad = idActividad;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getIdActividad() {
        return idActividad;
    }

    public int getValor() {
        return valor;
    }
    @Override
    public String toString() {
        return this.descripcion;
    }
    
   
}
