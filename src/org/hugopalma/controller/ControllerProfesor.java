/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Profesor;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerProfesor {
    
    private static final ControllerProfesor CONTROLLER_PROFESOR = new 
        ControllerProfesor();
    
   private ArrayList<Profesor> arrayList;
    private ControllerProfesor() {
        arrayList = new ArrayList<>();
        getArrayList();
    }
    public static ControllerProfesor getCONTROLLER_PROFESOR() {
        return CONTROLLER_PROFESOR;
    }
    
    public void add(String nombre, String direccion, String dpi, int idMateria) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Profesor(nombre, direccion, dpi, idMateria) VALUES('" + nombre + "', '" + direccion+ "', '" + dpi + "', " +idMateria+ ")");
    }
    
    public ArrayList<Profesor> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("SELECT * FROM  Profesor");
        
        try {
            while (resultSet.next()) {
                Profesor profesor = new Profesor();
                profesor.setIdProfesor(resultSet.getInt("idProfesor"));
                profesor.setNombre(resultSet.getString("nombre"));
                profesor.setDireccion(resultSet.getString("direccion"));
                profesor.setDpi(resultSet.getString("dpi"));
                profesor.setMateria(ControllerMateria.getCONTROLLER_MATERIA().search(resultSet.getInt("idMateria")));
                arrayList.add(profesor);
                
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerProfesor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList;
    
    
    } 
    
    public ArrayList<Profesor> buscar(String texto) {
        ArrayList<Profesor> listaResultado = new ArrayList<>();
        for (Profesor profesor: arrayList) {
            if (profesor.getNombre().equals(texto) || String.valueOf(profesor.getIdProfesor()).equals(texto))  {
                listaResultado.add(profesor);
                return listaResultado;
            }
        }
        return null;
    }
    
    public void modify(String nombre, String direccion, String dpi, int materia, int idProfesor) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Profesor SET nombre = '" + nombre + "', direccion = '" + direccion + "', dpi = '" + dpi + "', idMateria = " + materia + " WHERE idProfesor = " + idProfesor +  ";");
    
    }
    
    public void delete (int idProfesor) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Profesor WHERE idProfesor = " + idProfesor + ";" );
    }
    
}
