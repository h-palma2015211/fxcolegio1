/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.SeccionAlumno;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerSeccionAlumno {
     private static final ControllerSeccionAlumno
            CONTROLLER_SECCION_ALUMNO = 
            new ControllerSeccionAlumno();
    private ArrayList<SeccionAlumno> arrayList;
   
    private ControllerSeccionAlumno() {
        this.arrayList = new ArrayList<>();
        
    }
    public static ControllerSeccionAlumno getControllerSeccionAlumno() {
        return CONTROLLER_SECCION_ALUMNO;
    }
    
    public ArrayList<SeccionAlumno> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM SeccionAlumnoJornada;");
       
        try {
            while (resultSet.next()) {
                SeccionAlumno seccionAlumno = new SeccionAlumno();
                seccionAlumno.setIdSeccionAlumno(resultSet.getInt("idSeccionAlumnoJornada")); 
                seccionAlumno.setIdAlumno(ControllerAlumno.getCONTROLLER_ALUMNO().search(resultSet.getInt("idAlumno")));
                seccionAlumno.setIdSeccion(ControllerSeccion.getCONTROLLER_SECCION().search(resultSet.getInt("idSeccion"))); 
                arrayList.add(seccionAlumno);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerSeccionAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList;
     
        
        
    }
    
    
    public void add(int idSeccion, int idAlumno) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO  SeccionAlumno (idSeccion, idAlumno) VALUES(" +idSeccion+ ", " +idAlumno+ ")");
    }
    
    public void modify(int idSeccion, int idAlumno, int idJornada, int id) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE SeccionAlumno SET idSeccion = " +idSeccion+ ", idAlumno =" +idAlumno+ "  WHERE idSeccionAlumnoJornada= " +id+ ";");
    }
    
    public void delete(int id) {
       
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM SeccionAlumno WHERE idAlumno= " +id+ ";");
        
    }
    
    
    
}
