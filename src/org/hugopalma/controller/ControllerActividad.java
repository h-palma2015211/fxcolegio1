/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Actividad;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerActividad {
    private static final ControllerActividad CONTROLLLER_ACTIVIDAD = new
        ControllerActividad();
   
     private ArrayList<Actividad> arrayList;
     
    private ControllerActividad() {
        this.arrayList = new ArrayList<>();
    }
    public static ControllerActividad getCONTROLLER_ACTIVIDAD() {
        return CONTROLLLER_ACTIVIDAD;
    }
   
    
    public void add(String descripcion, int valor, short bimestre, int idMateria) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Actividad(descripcion, valor, bimestre, idMateria) VALUES ('" + descripcion + "', " + valor +", " + bimestre + ", " +idMateria+ " )");
    }
    
    public ArrayList<Actividad> buscar(String texto){
        ArrayList<Actividad> lista = new ArrayList<>();
        for (Actividad actividad : arrayList) {
            if(actividad.getDescripcion().equals(texto) || String.valueOf(actividad.getIdActividad()).equals(texto)) {
                lista.add(actividad);
                return lista;
            } 
        }
        return null;
    
    }
    
    public ArrayList<Actividad> getArraylist() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("SELECT * FROM Actividad;");
    
        try {
            while (resultSet.next()) {
                Actividad actividad  = new Actividad();
                actividad.setIdActividad(resultSet.getInt("idActividad"));
                actividad.setDescripcion(resultSet.getString("descripcion"));
                actividad.setValor(resultSet.getInt("valor"));
                actividad.setBimestre(resultSet.getShort("bimestre"));
                actividad.setMateria(ControllerMateria.getCONTROLLER_MATERIA().search(resultSet.getInt("idMateria"))); 
                arrayList.add(actividad);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividad.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;
        
    }
    
    public void modify(String descripcion, int valor, short bimestre, int idMateria, int idActividad) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Actividad SET descripcion = '" + descripcion + "', valor = " + valor + ", bimestre = " +bimestre+ ", idMateria = " +idMateria+ " WHERE idActividad = " + idActividad +  ";");
    }
    
  
    public void delete(int idActividad) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Actividad WHERE idActividad = " + idActividad + ";");
    }
    
    
    public ArrayList<Actividad> getArrayList2(int idAlumno, int idMateria) {
       ArrayList<Actividad> array = new ArrayList<>();
        ResultSet result = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Actividad WHERE Actividad.idActividad IN(SELECT ActividadAlumno.idActividad FROM ActividadAlumno WHERE ActividadAlumno.idAlumno = " +idAlumno+ ")AND idMateria = " +idMateria+";");
        
        
        try {
            while (result.next()) {
              Actividad actividad = new Actividad();
              actividad.setIdActividad(result.getInt("idActividad"));
              actividad.setDescripcion(result.getString("descripcion"));
              array.add(actividad);
              
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return array;
    }

    
    public ArrayList<Actividad> getArrayList3(int idAlumno, int idMateria) {
       ArrayList<Actividad> array2 = new ArrayList<>();
        ResultSet result = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Actividad WHERE Actividad.idActividad  NOT IN(SELECT ActividadAlumno.idActividad FROM ActividadAlumno WHERE ActividadAlumno.idAlumno = " +idAlumno+ ") AND idMateria= " +idMateria+ " ;");
        
        
        try {
            while (result.next()) {
              Actividad actividad = new Actividad();
              actividad.setIdActividad(result.getInt("idActividad"));
              actividad.setDescripcion(result.getString("descripcion"));
              array2.add(actividad);
              
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return array2;
    }
    
    public Actividad search(int idActividad) {
        for (Actividad actividad : arrayList) {
            if (actividad.getIdActividad() == idActividad) {
                return actividad;
            }
        }
        return null;
    }
    
    
}
