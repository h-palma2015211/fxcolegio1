/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Grado;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerGrado {
    private static final ControllerGrado CONTROLLER_GRADO 
            = new ControllerGrado();
    
    private ArrayList<Grado> arrayList;
    private ControllerGrado() {
        this.arrayList = new ArrayList<>();
        getArrayList();
    }
    
    public static ControllerGrado getCONTROLLER_GRADO() {
        return CONTROLLER_GRADO;
    }
    
    
    public void add(String nombre) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Grado(nombre) VALUES ('" + nombre + "')");
    }
    
    public ArrayList<Grado> buscar(String criterio) {
        ArrayList<Grado> listaResultado = new ArrayList<>();
        for (Grado grado : arrayList) {
            if (grado.getNombre().equals(criterio) || String.valueOf(grado.getIdGrado()).equals(criterio)) {
                listaResultado.add(grado);
                return listaResultado;
                
            }
        }
        return null;
    }
    
    public ArrayList<Grado> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("SELECT * FROM Grado");
       
        try {
            while (resultSet.next()) {
                Grado grado = new Grado();
                grado.setIdGrado(resultSet.getInt("idGrado"));
                grado.setNombre(resultSet.getString("nombre"));
                arrayList.add(grado);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerGrado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;
    }
    
    public void modify(String nombre, int id) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Grado SET nombre = '" + nombre + "' WHERE idGrado = " + id +  ";" );
    }
    
    public void delete(int id) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Grado WHERE idGrado = " + id + ";");
    }
    
    
    public Grado search(int idGrado) {
        for (Grado grado : arrayList) {
            if (grado.getIdGrado() == idGrado) {
                return grado;
            
            }
        }
        return null;
    } 
    
    
    
    
    
    
}
