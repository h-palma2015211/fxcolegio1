/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Materia;
import org.hugopalma.conecction.SQLDatabaseConnection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HUAL
 */
public class ControllerMateria {
    private static final ControllerMateria CONTROLLER_MATERIA = new
        ControllerMateria();
    
    private ArrayList<Materia> arrayList;
    
    private ResultSet resultSet;
    private ControllerMateria() {
        this.arrayList = new ArrayList<>();
        getArrayList();
    }
    
    public static ControllerMateria getCONTROLLER_MATERIA() {
        return CONTROLLER_MATERIA;
    }
    public void add(String nombre) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Materia(nombre) VALUES ('" + nombre + "')");
    }
    
    public ArrayList<Materia> getArrayMateria(String buscar) {
        arrayList.clear();
        ArrayList<Materia> arraylistRetorno = new ArrayList<>();
        for (Materia busqueda: arrayList) {
            if (busqueda.getNombre().equals(buscar)) {
                arraylistRetorno.add(busqueda);
                return arraylistRetorno;
            }
        }
        
        return null;
    
    }
    
    public ArrayList<Materia> getArrayList(){
        arrayList.clear();
        
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("SELECT * FROM Materia;");
        
        try {
            while (resultSet.next()) {
                Materia materia = new Materia();
                materia.setIdMateria(resultSet.getInt("idMateria"));
                materia.setNombre(resultSet.getString("nombre"));
                arrayList.add(materia);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerMateria.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;
    }
    
    public void modify(String nombre, int idMateria) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Usuario SET nombre =  nombre = '" + nombre + "'WHERE idUsuario = " + idMateria +  ";");
    }
    
    public void delete(int idMateria) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Materia WHERE idMateria = " + idMateria + ";");
    
    }
    
    
    public ArrayList<Materia> buscar(String texto) {
       ArrayList<Materia> lista = new ArrayList<>();
       for (Materia materia: arrayList) {
           if (materia.getNombre().equals(texto) || String.valueOf(materia.getIdMateria()).equals(texto)) {
           lista.add(materia);
           return lista;
       }
    }
       return null;
}


    public Materia search(int idMateria) {
        for(Materia materia : arrayList) {
            if (materia.getIdMateria() == idMateria) {
           
                return materia;
                
            }
        }
        
        return null;
    
    }
}