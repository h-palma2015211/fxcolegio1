/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Seccion;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerSeccion {
    private static final ControllerSeccion CONTROLLER_SECCION = 
            new ControllerSeccion();
    
    private ArrayList<Seccion> arrayList;
    private ArrayList<Seccion> arrayList2;
    private ArrayList<Seccion> arrayList3;
    private ControllerSeccion() {
        this.arrayList2 = new ArrayList<>();
        this.arrayList3 = new ArrayList<>();
        this.arrayList = new ArrayList<>();
        getArrayList();
    }
    public static ControllerSeccion getCONTROLLER_SECCION() {
        return CONTROLLER_SECCION;
    }
    
    public void add(String nombre, int idGrado, int idJornada) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Seccion (nombre, idGrado, idJornada) VALUES('" +nombre+ "', " + idGrado+ ", " +idJornada+ ")");
    
    }
    
    public ArrayList<Seccion> buscar(String criterio) {
        ArrayList<Seccion> lista = new ArrayList<>();
        for (Seccion seccion : arrayList) {
            if (seccion.getNombre().equals(criterio) || String.valueOf(seccion.getIdSeccion()).equals(criterio)) {
                lista.add(seccion);
                return lista;
            }
        }
        return null;
    }
    
    public void modify(String nombre,int idGrado, int idSeccion, int idJornada) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery(" UPDATE Seccion SET nombre= '" + nombre + "', idGrado = " + idGrado + ", idJoranda = " +idJornada+ " WHERE idSeccion = " +idSeccion+ ";");

    }
    
    public void delete(int idSeccion) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Seccion WHERE idSeccion = " + idSeccion + ";");
    }
    public ArrayList<Seccion> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("SELECT * FROM Seccion;");
        
        
        try {
            while (resultSet.next()) {
                Seccion seccion = new Seccion();
                seccion.setIdSeccion(resultSet.getInt("idSeccion"));
                seccion.setNombre(resultSet.getString("nombre"));
                seccion.setGrado(ControllerGrado.getCONTROLLER_GRADO().search(resultSet.getInt("idGrado")));
                seccion.setJornada(ControllerJornada.getCONTROLLER_JORNADA().search(resultSet.getInt("idJornada")));
                arrayList.add(seccion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerSeccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;
    }
    
    public Seccion search(int idSeccion) {
        for (Seccion seccion : arrayList) {
            if (seccion.getIdSeccion() == idSeccion) {
                return seccion;
            }
        }
        return null;
    }
    
    
    public ArrayList<Seccion> getArrayList2(int idProfesor) {
        arrayList2.clear();
        ResultSet result = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Seccion WHERE Seccion.idSeccion  IN(SELECT MateriaSeccionProfesor.idSeccion FROM MateriaSeccionProfesor WHERE MateriaSeccionProfesor.idProfesor = " +idProfesor+ ")");
        
        try {
            while (result.next()) {
                Seccion seccion = new Seccion();
                seccion.setIdSeccion(result.getInt("idSeccion"));
                seccion.setNombre(result.getString("nombre"));
                arrayList2.add(seccion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerSeccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList2;
    }
    
    
    
public ArrayList<Seccion> getArrayList3(int idProfesor) {
        arrayList3.clear();
        ResultSet result = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Seccion WHERE Seccion.idSeccion NOT IN(SELECT MateriaSeccionProfesor.idSeccion FROM MateriaSeccionProfesor WHERE MateriaSeccionProfesor.idProfesor = " +idProfesor+ ")");
        
        try {
            while (result.next()) {
                Seccion seccion = new Seccion();
                seccion.setIdSeccion(result.getInt("idSeccion"));
                seccion.setNombre(result.getString("nombre"));
                arrayList3.add(seccion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerSeccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList3;
    }
    
}
