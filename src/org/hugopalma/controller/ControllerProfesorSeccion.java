/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.util.ArrayList;
import org.hugopalma.bean.MateriaSeccionProfesor;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerProfesorSeccion {
    private static final ControllerProfesorSeccion CONTROLLER_PROFESOR_SECCION 
            = new ControllerProfesorSeccion();
    
    private ArrayList<MateriaSeccionProfesor> arrayList;
    
    private ControllerProfesorSeccion() {
        this.arrayList = new ArrayList<>();
    }
    
    public static ControllerProfesorSeccion getCONTROLLER_PROFESOR_SECCION() {
        return CONTROLLER_PROFESOR_SECCION;
    }
    public void add(int idSeccion, int idProfesor) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO MateriaSeccionProfesor (idSeccion, idProfesor) VALUES(" +idSeccion+ ", " +idProfesor+ ")");
    }
    
    public void delete(int id) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM MateriaSeccionProfesor WHERE idSeccion= " +id+ ";");
    } 
}
