/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.conecction;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hugopalma.util.PropertiesLoader;

/**
 *
 * @author HUAL
 */
public class SQLDatabaseConnection {
   public static final SQLDatabaseConnection SQL_DATABASE_CONNECTION = 
            new SQLDatabaseConnection();
    
    private String connectionString;
    private Connection connection;
    private ResultSet resultSet;
    private Statement statement;
    private PreparedStatement preparedStatement;
    
    private SQLDatabaseConnection() {
    }
    
    public static SQLDatabaseConnection getSQL_DATABASE_CONNECTION() {
        return SQL_DATABASE_CONNECTION;
    } 
    
    public void connect() {
        HashMap<String, String> hashMap = PropertiesLoader.getPROPERTIES_LOADER().load("connection.properties");
        connectionString = "jdbc:sqlserver://" + hashMap.get("serverName") + ":" + hashMap.get("portNumber") + ";"
            + "databaseName=" + hashMap.get("databaseName")+ ";"
            + "user=" + hashMap.get("user") + ";"
            + "password=" + hashMap.get("password") + ";" 
            + "encript=false;"
            + "trustServerCertificate=false;"
            + "loginTimeout=30;";
        
        try {
            connection = DriverManager.getConnection(connectionString);
        } catch (SQLException ex) {
            Logger.getLogger(SQLDatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public ResultSet query(String query) {
        connect();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(SQLDatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return resultSet;
    }
        
    public void disconnect() {
        if (resultSet != null) 
            try { resultSet.close(); } catch(Exception e) {}
        if (statement != null) 
            try { statement.close(); } catch(Exception e) {}
        if (connection != null) 
            try { connection.close(); } catch(Exception e) {}
    } 
    
    public void executeQuery(String query) {
        connect();
        
       try {
           preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
           preparedStatement.execute();
           resultSet = preparedStatement.getGeneratedKeys();
           
           while (resultSet.next()) {
               System.out.println("Generado: " + resultSet.getString(1));
           }
       
       } catch (SQLException ex) {
           Logger.getLogger(SQLDatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
       }
        
                        
    }
    
}
